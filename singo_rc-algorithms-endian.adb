------------------------------------------------------------------------------
-- Copyright (c) <2009>, <Singo Solution>, <AuroraUX Group>                 --
-- All rights reserved.                                                     --
--                                                                          --
-- Redistribution  and use  in source  and binary  forms,  with or without  --
-- modification,are permitted provided that the following conditions are met--
--                                                                          --
--     * Redistributions of source code must retain the above copyright     --
--       notice, this list of conditions and the following disclaimer.      --
--     * Redistributions in binary form must reproduce the above copyright  --
--       notice, this list of conditions and the following disclaimer in the--
--       documentation and/or other materials provided with the distribution--
--     * Neither the name of the <AuroraUX Group> nor the names of its      --
--       contributors may be used to endorse or promote products derived    --
--       from this software without specific prior written permission.      --
--                                                                          --
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS  --
-- IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,--
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR   --
-- PURPOSE ARE  DISCLAIMED.  IN NO  EVENT  SHALL THE  COPYRIGHT  HOLDER OR  --
-- CONTRIBUTORS  BE LIABLE FOR ANY DIRECT,  INDIRECT, INCIDENTAL, SPECIAL,  --
-- EXEMPLARY,  OR CONSEQUENTIAL  DAMAGES  (INCLUDING, BUT NOT LIMITED TO,   --
-- PROCUREMENT  OF SUBSTITUTE  GOODS OR  SERVICES;  LOSS OF USE,  DATA, OR  --
-- PROFITS; OR BUSINESS INTERRUPTION)  HOWEVER CAUSED AND ON ANY THEORY OF  --
-- LIABILITY,  WHETHER IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  --
-- NEGLIGENCE  OR  OTHERWISE) ARISING  IN ANY  WAY OUT  OF THE USE OF THIS  --
-- SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.             --
------------------------------------------------------------------------------

with Ada.Unchecked_Conversion;
with System.Storage_Elements;

package body Singo_RC.Algorithms.Endian is
   Dummy : Discrete; -- This exists solely to calculate Byte_Size.

   use type System.Storage_Elements.Storage_Offset;

   Byte_Size : constant System.Storage_Elements.Storage_Offset := (Dummy'Size + System.Storage_Unit - 1) / System.Storage_Unit;

   function Swap (From : Discrete) return Discrete is
      subtype Byte_List is System.Storage_Elements.Storage_Array (1 .. Byte_Size);

      function To_Bytes    is new Ada.Unchecked_Conversion (Source => Discrete,  Target => Byte_List);
      function To_Discrete is new Ada.Unchecked_Conversion (Source => Byte_List, Target => Discrete);

      Byte : Byte_List                              := To_Bytes (From);
      Last : System.Storage_Elements.Storage_Offset := Byte_Size;
      Temp : System.Storage_Elements.Storage_Element;
   begin -- Swap
      All_Pairs : for First in Byte'First .. Byte_Size / 2 loop
         Temp         := Byte (First);
         Byte (First) := Byte (Last);
         Byte (Last)  := Temp;

         Last := Last - 1;
      end loop All_Pairs;

      return To_Discrete (Byte);
   end Swap;

   use type System.Bit_Order;

   function To_Big (From : Discrete) return Discrete is
   begin -- To_Big
      if System.Default_Bit_Order = System.Low_Order_First then
         return Swap (From);
      end if;

      return From;
   end To_Big;

   function To_Little (From : Discrete) return Discrete is
   begin -- To_Little
      if System.Default_Bit_Order = System.High_Order_First then
         return Swap (From);
      end if;

      return From;
   end To_Little;
end Singo_RC.Algorithms.Endian;
