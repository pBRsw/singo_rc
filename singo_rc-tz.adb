------------------------------------------------------------------------------
-- Copyright (c) <2009>, <Singo Solution>, <W M Richards>, <AuroraUX Group> --
-- All rights reserved.                                                     --
--                                                                          --
-- Redistribution  and use  in source  and binary  forms,  with or without  --
-- modification,are permitted provided that the following conditions are met--
--                                                                          --
--     * Redistributions of source code must retain the above copyright     --
--       notice, this list of conditions and the following disclaimer.      --
--     * Redistributions in binary form must reproduce the above copyright  --
--       notice, this list of conditions and the following disclaimer in the--
--       documentation and/or other materials provided with the distribution--
--     * Neither the name of the <AuroraUX Group> nor the names of its      --
--       contributors may be used to endorse or promote products derived    --
--       from this software without specific prior written permission.      --
--                                                                          --
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS  --
-- IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,--
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR   --
-- PURPOSE ARE  DISCLAIMED.  IN NO  EVENT  SHALL THE  COPYRIGHT  HOLDER OR  --
-- CONTRIBUTORS  BE LIABLE FOR ANY DIRECT,  INDIRECT, INCIDENTAL, SPECIAL,  --
-- EXEMPLARY,  OR CONSEQUENTIAL  DAMAGES  (INCLUDING, BUT NOT LIMITED TO,   --
-- PROCUREMENT  OF SUBSTITUTE  GOODS OR  SERVICES;  LOSS OF USE,  DATA, OR  --
-- PROFITS; OR BUSINESS INTERRUPTION)  HOWEVER CAUSED AND ON ANY THEORY OF  --
-- LIABILITY,  WHETHER IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  --
-- NEGLIGENCE  OR  OTHERWISE) ARISING  IN ANY  WAY OUT  OF THE USE OF THIS  --
-- SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.             --
------------------------------------------------------------------------------

with Interfaces.C.Strings;

package body Singo_RC.TZ is
   package C renames Interfaces.C;
   package S renames Interfaces.C.Strings;

   -- Create a type so we can fetch the zone-name values.
   type TZName_Ptrs is array (1 .. 2) of S.chars_ptr;

   -- External variables set/exported by the C tzset function.
   Tzname   : TZName_Ptrs;
   Timezone : C.long;
   Daylight : C.int;

   pragma Import (C, Tzname,   "tzname");
   pragma Import (C, Timezone, "timezone");
   pragma Import (C, Daylight, "daylight");

   function TZ_Name (DST : Boolean := False) return String is
   begin -- TZ_Name
      TZ_Set;

      if DST then
         return S.Value (Tzname (2) );
      else
         return S.Value (Tzname (1) );
      end if;
   end TZ_Name;

   function Zone_Offset return Natural is
   begin -- Zone_Offset
      TZ_Set;

      return Natural (Timezone);
   end Zone_Offset;

   function Does_DST return Boolean is
      use type C.int;
   begin -- Does_DST
      TZ_Set;

      return Daylight /= 0;
   end Does_DST;
end Singo_RC.TZ;
