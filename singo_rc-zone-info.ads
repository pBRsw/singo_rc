------------------------------------------------------------------------------
-- Copyright (c) <2009>, <Singo Solution>, <AuroraUX Group>                 --
-- All rights reserved.                                                     --
--                                                                          --
-- Redistribution  and use  in source  and binary  forms,  with or without  --
-- modification,are permitted provided that the following conditions are met--
--                                                                          --
--     * Redistributions of source code must retain the above copyright     --
--       notice, this list of conditions and the following disclaimer.      --
--     * Redistributions in binary form must reproduce the above copyright  --
--       notice, this list of conditions and the following disclaimer in the--
--       documentation and/or other materials provided with the distribution--
--     * Neither the name of the <AuroraUX Group> nor the names of its      --
--       contributors may be used to endorse or promote products derived    --
--       from this software without specific prior written permission.      --
--                                                                          --
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS  --
-- IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,--
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR   --
-- PURPOSE ARE  DISCLAIMED.  IN NO  EVENT  SHALL THE  COPYRIGHT  HOLDER OR  --
-- CONTRIBUTORS  BE LIABLE FOR ANY DIRECT,  INDIRECT, INCIDENTAL, SPECIAL,  --
-- EXEMPLARY,  OR CONSEQUENTIAL  DAMAGES  (INCLUDING, BUT NOT LIMITED TO,   --
-- PROCUREMENT  OF SUBSTITUTE  GOODS OR  SERVICES;  LOSS OF USE,  DATA, OR  --
-- PROFITS; OR BUSINESS INTERRUPTION)  HOWEVER CAUSED AND ON ANY THEORY OF  --
-- LIABILITY,  WHETHER IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  --
-- NEGLIGENCE  OR  OTHERWISE) ARISING  IN ANY  WAY OUT  OF THE USE OF THIS  --
-- SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.             --
------------------------------------------------------------------------------

-- Simple timezone services based on zoneinfo data

with Ada.Calendar.Time_Zones;

package Singo_RC.Zone.Info is
   function UTC_Time_Offset (Zone_Name : String; Date : Ada.Calendar.Time := Ada.Calendar.Clock; Zone_Dir : String := "")
   return Ada.Calendar.Time_Zones.Time_Offset;
   -- Returns, as a number of minutes, the difference between the given time
   -- zone and UTC time, at the time Date.  If data for the time zone cannot
   -- be loaded from the zoneinfo database, then Unknown_Zone_Error from
   -- Ada.Calendar.Time_Zones is raised.
   --
   -- The Zone_Dir value, if given, is used as the location of the zoneinfo
   -- database.  If the given zone is not found there, then checks the TZDIR
   -- environment variable.  If no find in either of those, checks a built-in
   -- list of directories.
end Singo_RC.Zone.Info;
