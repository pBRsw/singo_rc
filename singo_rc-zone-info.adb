------------------------------------------------------------------------------
-- Copyright (c) <2009>, <Singo Solution>, <AuroraUX Group>                 --
-- All rights reserved.                                                     --
--                                                                          --
-- Redistribution  and use  in source  and binary  forms,  with or without  --
-- modification,are permitted provided that the following conditions are met--
--                                                                          --
--     * Redistributions of source code must retain the above copyright     --
--       notice, this list of conditions and the following disclaimer.      --
--     * Redistributions in binary form must reproduce the above copyright  --
--       notice, this list of conditions and the following disclaimer in the--
--       documentation and/or other materials provided with the distribution--
--     * Neither the name of the <AuroraUX Group> nor the names of its      --
--       contributors may be used to endorse or promote products derived    --
--       from this software without specific prior written permission.      --
--                                                                          --
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS  --
-- IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,--
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR   --
-- PURPOSE ARE  DISCLAIMED.  IN NO  EVENT  SHALL THE  COPYRIGHT  HOLDER OR  --
-- CONTRIBUTORS  BE LIABLE FOR ANY DIRECT,  INDIRECT, INCIDENTAL, SPECIAL,  --
-- EXEMPLARY,  OR CONSEQUENTIAL  DAMAGES  (INCLUDING, BUT NOT LIMITED TO,   --
-- PROCUREMENT  OF SUBSTITUTE  GOODS OR  SERVICES;  LOSS OF USE,  DATA, OR  --
-- PROFITS; OR BUSINESS INTERRUPTION)  HOWEVER CAUSED AND ON ANY THEORY OF  --
-- LIABILITY,  WHETHER IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  --
-- NEGLIGENCE  OR  OTHERWISE) ARISING  IN ANY  WAY OUT  OF THE USE OF THIS  --
-- SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.             --
------------------------------------------------------------------------------

-- Simple timezone services based on zoneinfo data

with Ada.Unchecked_Conversion;
with Singo_RC.Zone.File;

package body Singo_RC.Zone.Info is
   function UTC_Time_Offset (Zone_Name : String; Date : Ada.Calendar.Time := Ada.Calendar.Clock; Zone_Dir : String := "")
   return Ada.Calendar.Time_Zones.Time_Offset is
      Epoch_Date : constant Ada.Calendar.Time := Ada.Calendar.Time_Of (1970, 01, 01);

      State : Singo_RC.Zone.File.State_Info;
      Now   : Epoch_Value;
      Pick  : Natural;
      Lo    : Positive;
      Hi    : Positive;
      Mid   : Positive;

      use type Ada.Calendar.Time;
   begin  -- UTC_Time_Offset
      -- Start by fetching the zoneinfo data for the given timezone.
      Singo_RC.Zone.File.Load (Zone_Name => Zone_Name, Zone_State => State, Zone_Dir => Zone_Dir);

      -- Use the given epoch time to pick a transition offset.
      Now := Epoch_Value (Date - Epoch_Date);

      -- If there are no transition times, or if the current time is before
      -- the very first one, then scan for the first non-DST one.
      if State.Time_Count = 0 or Now < State.Trans_Times (0).Transition_At then
         Pick := 0;

         Set_Pick : loop
            exit Set_Pick when not State.Type_Infos (Pick).Is_DST;

            Pick := Pick + 1;

            -- If we didn't find any non-DST ones, then just fall back to the
            -- first one.
            if Pick >= State.Type_Count then
               Pick := 0;

               exit Set_Pick;
            end if;
         end loop Set_Pick;
      else
         -- Do a binary search of the transition times to find the first one
         -- that's larger than the current time.
         Lo := 1;
         Hi := State.Time_Count;

         Search : loop
            exit Search when Lo >= Hi;

            Mid := (Lo + Hi) / 2;

            if Now < State.Trans_Times (Mid).Transition_At then
               Hi := Mid;
            else
               Lo := Mid + 1;
            end if;
         end loop Search;

         -- Pick the transition time just before the first one that's larger
         -- than the current time, which would be the last one that's smaller
         -- than current.
         Pick := State.Trans_Times (Lo - 1).Transition_Type;
      end if;

      -- Having made our choice, convert from seconds to minutes and report it
      -- to the caller.
      return Ada.Calendar.Time_Zones.Time_Offset (State.Type_Infos (Pick).GMT_Offset / 60);
   exception -- UTC_Time_Offset
      when Zone.File.Unknown_Zone_Error => -- Map the internal exception to the standard one.
         raise Ada.Calendar.Time_Zones.Unknown_Zone_Error;
   end UTC_Time_Offset;
end Singo_RC.Zone.Info;
