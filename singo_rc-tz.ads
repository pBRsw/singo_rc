------------------------------------------------------------------------------
-- Copyright (c) <2009>, <Singo Solution>, <W M Richards>, <AuroraUX Group> --
-- All rights reserved.                                                     --
--                                                                          --
-- Redistribution  and use  in source  and binary  forms,  with or without  --
-- modification,are permitted provided that the following conditions are met--
--                                                                          --
--     * Redistributions of source code must retain the above copyright     --
--       notice, this list of conditions and the following disclaimer.      --
--     * Redistributions in binary form must reproduce the above copyright  --
--       notice, this list of conditions and the following disclaimer in the--
--       documentation and/or other materials provided with the distribution--
--     * Neither the name of the <AuroraUX Group> nor the names of its      --
--       contributors may be used to endorse or promote products derived    --
--       from this software without specific prior written permission.      --
--                                                                          --
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS  --
-- IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,--
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR   --
-- PURPOSE ARE  DISCLAIMED.  IN NO  EVENT  SHALL THE  COPYRIGHT  HOLDER OR  --
-- CONTRIBUTORS  BE LIABLE FOR ANY DIRECT,  INDIRECT, INCIDENTAL, SPECIAL,  --
-- EXEMPLARY,  OR CONSEQUENTIAL  DAMAGES  (INCLUDING, BUT NOT LIMITED TO,   --
-- PROCUREMENT  OF SUBSTITUTE  GOODS OR  SERVICES;  LOSS OF USE,  DATA, OR  --
-- PROFITS; OR BUSINESS INTERRUPTION)  HOWEVER CAUSED AND ON ANY THEORY OF  --
-- LIABILITY,  WHETHER IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  --
-- NEGLIGENCE  OR  OTHERWISE) ARISING  IN ANY  WAY OUT  OF THE USE OF THIS  --
-- SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.             --
------------------------------------------------------------------------------

package Singo_RC.TZ is
   procedure TZ_Set;
   pragma Import (C, TZ_Set, "tzset");
   -- Binding to the standard POSIX procedure to set local timezone info,
   -- which can then be retrieved by the following functions.  Since all the
   -- functions in this package call TZ_Set themselves, one need only call it
   -- directly when you want to affect other services, such as Ada.Calendar.

   function TZ_Name (DST : Boolean := False) return String;
   -- Returns either the canonical name of the normal (non daylight savings
   -- time) local timezone (eg. "EST") if the DST parameter is false, or the
   -- DST zone name (eg. "EDT") if it's true.

   function Zone_Offset return Natural;
   -- Returns the current offset in seconds of local timezone from GMT
   -- ("seconds west of GMT").

   function Does_DST return Boolean;
   -- Returns true if local timezone does daylight savings time, false if it
   -- does not.  Note that this does not indicate whether DST is currently in
   -- effect, only if it is ever observed in the local zone.
end Singo_RC.TZ;
