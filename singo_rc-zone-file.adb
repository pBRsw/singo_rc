------------------------------------------------------------------------------
-- Copyright (c) <2009>, <Singo Solution>, <AuroraUX Group>                 --
-- All rights reserved.                                                     --
--                                                                          --
-- Redistribution  and use  in source  and binary  forms,  with or without  --
-- modification,are permitted provided that the following conditions are met--
--                                                                          --
--     * Redistributions of source code must retain the above copyright     --
--       notice, this list of conditions and the following disclaimer.      --
--     * Redistributions in binary form must reproduce the above copyright  --
--       notice, this list of conditions and the following disclaimer in the--
--       documentation and/or other materials provided with the distribution--
--     * Neither the name of the <AuroraUX Group> nor the names of its      --
--       contributors may be used to endorse or promote products derived    --
--       from this software without specific prior written permission.      --
--                                                                          --
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS  --
-- IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,--
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR   --
-- PURPOSE ARE  DISCLAIMED.  IN NO  EVENT  SHALL THE  COPYRIGHT  HOLDER OR  --
-- CONTRIBUTORS  BE LIABLE FOR ANY DIRECT,  INDIRECT, INCIDENTAL, SPECIAL,  --
-- EXEMPLARY,  OR CONSEQUENTIAL  DAMAGES  (INCLUDING, BUT NOT LIMITED TO,   --
-- PROCUREMENT  OF SUBSTITUTE  GOODS OR  SERVICES;  LOSS OF USE,  DATA, OR  --
-- PROFITS; OR BUSINESS INTERRUPTION)  HOWEVER CAUSED AND ON ANY THEORY OF  --
-- LIABILITY,  WHETHER IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  --
-- NEGLIGENCE  OR  OTHERWISE) ARISING  IN ANY  WAY OUT  OF THE USE OF THIS  --
-- SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.             --
------------------------------------------------------------------------------

-- Low-level package for reading zoneinfo data files

with Ada.Characters.Latin_1;
with Ada.Directories;
with Ada.Environment_Variables;
with Ada.Streams.Stream_IO;
with Ada.Strings.Fixed;
with Ada.Unchecked_Conversion;

with Singo_RC.Binary;
with Singo_RC.Algorithms.Endian;

package body Singo_RC.Zone.File is
   package Binary renames Singo_RC.Binary;

   Dirsep             : constant Character := '/';  -- the Unix fave
   Max_Dirname_Length : constant := 25; -- Adjust if table is changed.

   subtype Zoneinfo_DB_Dirname is String (1 .. Max_Dirname_Length);

   type Zoneinfo_Path_Table is array (Positive range <>) of Zoneinfo_DB_Dirname;

   Zoneinfo_DB_Paths : constant Zoneinfo_Path_Table := ("/usr/share/zoneinfo      ",
                                                        "/usr/lib/zoneinfo        ",
                                                        "/usr/local/share/zoneinfo",
                                                        "/usr/local/lib/zoneinfo  ");
   -- The built-in lookup table of zoneinfo database directory paths.

   Magic_Len     : constant :=  4; -- Length in chars of header's "magic" string.
   Reserved_Len  : constant := 16; -- Length in bytes of RFU array.
   Version_Index : constant :=  1; -- Index within RFU array of version number.

   subtype Magic_String  is String (1 .. Magic_Len);
   subtype Reserved_Data is Binary.Byte_String (1 .. Reserved_Len);

   type TZFile_Header is record
      Magic      : Magic_String;
      Ver_Rsvd   : Reserved_Data;
      Is_GMT_Cnt : Binary.Word;
      Is_Std_Cnt : Binary.Word;
      Leap_Cnt   : Binary.Word;
      Time_Cnt   : Binary.Word;
      Type_Cnt   : Binary.Word;
      Char_Cnt   : Binary.Word;
   end record;

   Header_Len : constant Integer := TZFile_Header'Size / Binary.Byte_Bits;
   State_Len  : constant Integer := State_Info'Size    / Binary.Byte_Bits;
   Buffer_Len : constant Integer := (2 * Header_Len) + (2 * State_Len) + (4 * Max_Times);
   -- The header size is the same as the original C definition, but the other
   -- two sizes are a bit larger.  This is because we don't spend any effort
   -- to compact things like booleans or one-byte indices, and is okay as long
   -- as it *is* larger and not smaller, since they're only used to determine
   -- the buffer size.  The actual sizes of the data are accounted for during
   -- the extraction process in Load.

   subtype Buffer_Range  is Integer range 0 .. Buffer_Len - 1; -- index
   subtype Data_Size     is Integer range 0 .. Buffer_Len;     -- count
   subtype TZFile_Buffer is Binary.Byte_String (Buffer_Range);

   procedure Read_Data (File : in Ada.Streams.Stream_IO.File_Type; Buffer : out TZFile_Buffer; Bytes_Read : out Data_Size);
   -- Low-level procedure to gulp in an entire zoneinfo data file as a byte
   -- string.

   procedure Read_Data (File : in Ada.Streams.Stream_IO.File_Type; Buffer : out TZFile_Buffer; Bytes_Read : out Data_Size) is
      use Ada.Streams;
      use Ada.Streams.Stream_IO;

      subtype Buf_SEA is Stream_Element_Array (1 .. Stream_Element_Offset (Buffer_Len) );

      function SEA_To_Buf is new Ada.Unchecked_Conversion (Source => Buf_SEA, Target => TZFile_Buffer);
      -- These declarations allow using the Stream_IO.Read procedure, which is
      -- much faster than the 'Read stream attribute, but which wants to read
      -- into an array of Stream_Element, not Byte.  Even copying the entire
      -- buffer, this is faster.

      SEA  : Buf_SEA;
      Last : Stream_Element_Offset;
   begin -- Read_Data
      Read (File => File, Item => SEA, Last => Last);
      Buffer := SEA_To_Buf (SEA);
      Bytes_Read := Data_Size (Last) + 1;
   end Read_Data;

   procedure Load (Zone_Name : in String; Zone_State : out State_Info; Zone_Dir : in String := "") is
      use Binary;

      package Wds  is new Singo_RC.Algorithms.Endian (Discrete => Binary.Word);
      package Ints is new Singo_RC.Algorithms.Endian (Discrete => Binary.S_Word);

      Buf      : TZFile_Buffer; -- main input buffer
      Hdr      : TZFile_Header; -- typed header data
      Got      : Data_Size;     -- how much data we actually got
      Std_Cnt  : Natural;       -- two header fields that are used only locally
      GMT_Cnt  : Natural;
      Tmp_Byte : Byte;          -- holding area for byte-sized data

      Extract_Index : Buffer_Range;
      -- Cursor variable and extraction functions to allow simplified stepping
      -- through the raw data, extracting bytes and words and converting them
      -- into host-ordered standard data.

      function Extract_Word return Integer;
      -- Extract a word (32 bits) from the buffer, advance the cursor, and
      -- return the result in host byte order.

      function Extract_Word return Integer is
         function BS_To_S_Word is new Ada.Unchecked_Conversion (Source => Word_Byte_String, Target => S_Word);

         Raw_Word : S_Word;
      begin -- Extract_Word
         Raw_Word := BS_To_S_Word (Buf (Extract_Index .. Extract_Index + Word_Bytes - 1) );
         Extract_Index := Extract_Index + Word_Bytes;

         return Integer (Ints.From_Big (Raw_Word) );
      end Extract_Word;

      function Extract_Byte return Byte;
      -- Extract a byte from the buffer and advance the cursor.

      function Extract_Byte return Byte is
      begin -- Extract_Byte
         -- We increment first, then return the previous byte, so we don't
         -- have to store the byte in a temporary location.
         Extract_Index := Extract_Index + 1;

         return Buf (Extract_Index - 1);
      end Extract_Byte;

      function Find_Zoneinfo_File (Name : String; Dir : String) return String;
      -- Tries our potential database locations in our preferred order,
      -- looking for the given zoneinfo data file.  Order is:
      --
      -- 1. Given directory parameter.
      -- 2. TZDIR environment variable.
      -- 3. Built-in list of directory pathnames.
      --
      -- Raises the unknown-zone exception if no find anywhere, or returns the
      -- full pathname of the first find.

      function Find_Zoneinfo_File (Name : String; Dir : String) return String is
      begin -- Find_Zoneinfo_File
         -- Try the given directory path first
         declare
            Path : constant String := Dir & Dirsep & Name;
         begin
            if Dir /= "" and then Ada.Directories.Exists (Path) then
               return Path;
            end if;
         end;

         -- No path given or no file there, try the TZDIR environment
         -- variable.
         begin
            declare
               TZDIR : constant String := Ada.Environment_Variables.Value ("TZDIR");
               Path  : constant String := TZDIR & Dirsep & Name;
            begin
               if TZDIR /= "" and then Ada.Directories.Exists (Path) then
                  return Path;
               end if;
            end;
         exception
            when others => -- Usually means the EV isn't set, but whatever the problem, it
               null;       -- sure isn't a usable location.  Move on to the next check.
         end;

         -- No joy on previous checks, try each directory in our built-in
         -- list.
         for Next in Zoneinfo_DB_Paths'Range loop
            declare
               use Ada.Strings;

               Path : constant String := Fixed.Trim (Zoneinfo_DB_Paths (Next), Side => Right) & Dirsep & Name;
            begin
               if Ada.Directories.Exists (Path) then
                  return Path;
               end if;
            end;
         end loop;

         -- Checked every place we can think of without finding it, so that
         -- means this isn't a known zone name, or else the zoneinfo database
         -- isn't installed where we can find it.
         raise Unknown_Zone_Error;
      end Find_Zoneinfo_File;
   begin -- Load
      -- Find and open the file, converting all Stream_IO exceptions into our
      -- own local exception.  Read the entire file into the buffer, then
      -- close it.
      declare
         use Ada.Streams.Stream_IO;

         Path : constant String := Find_Zoneinfo_File (Zone_Name, Zone_Dir);

         File : File_Type;
      begin
         Open (File => File, Mode => In_File, Name => Path);
         Read_Data (File => File, Buffer => Buf, Bytes_Read => Got);
         Close (File => File);
      exception
         when others =>
            raise Unknown_Zone_Error;
      end;

      -- Convert header from a raw byte string to internal form.  Leaves the
      -- numbers in big-endian format; they will be converted later.
      declare
         subtype Hdr_BS is Byte_String (1 .. Header_Len);

         function BS_To_Hdr is new Ada.Unchecked_Conversion (Hdr_BS, TZFile_Header);
      begin
         Hdr := BS_To_Hdr (Buf (Buf'First .. Buf'First + Header_Len - 1) );
      end;

      -- Initial header validation: length, magic string, and version number.
      -- In these cases, the "unknown zone" error means that even though
      -- there's a matching file in the db, it's not a valid zoneinfo file.
      if Got < Header_Len then
         raise Unknown_Zone_Error;
      end if;

      if Hdr.Magic /= "TZif" then
         raise Unknown_Zone_Error;
      end if;

      if Hdr.Ver_Rsvd (Version_Index) /= 0 and Hdr.Ver_Rsvd (Version_Index) /= Character'Pos ('2') then
         raise Unknown_Zone_Error;
      end if;

      -- Extract and convert the header data into our result, the state-info
      -- record.  If any values are out of range, the header is invalid.
      begin
         Zone_State.Leap_Count := Natural (Wds.From_Big (Hdr.Leap_Cnt) );
         Zone_State.Time_Count := Natural (Wds.From_Big (Hdr.Time_Cnt) );
         Zone_State.Type_Count := Natural (Wds.From_Big (Hdr.Type_Cnt) );
         Zone_State.Char_Count := Natural (Wds.From_Big (Hdr.Char_Cnt) );
      exception
         when Constraint_Error =>
            raise Unknown_Zone_Error;
      end;

      -- Final header validation: count limits, data size, and data
      -- consistency.
      Std_Cnt := Natural (Wds.From_Big (Hdr.Is_Std_Cnt) );
      GMT_Cnt := Natural (Wds.From_Big (Hdr.Is_GMT_Cnt) );

      if Zone_State.Leap_Count > Max_Leaps                   or
         Zone_State.Type_Count = 0                           or
         Zone_State.Type_Count > Max_Types                   or
         Zone_State.Time_Count > Max_Times                   or
         Zone_State.Char_Count > Max_Chars                   or
         (Std_Cnt /= Zone_State.Type_Count and Std_Cnt /= 0) or
         (GMT_Cnt /= Zone_State.Type_Count and GMT_Cnt /= 0)
      then
         raise Unknown_Zone_Error;
      end if;

      if Got < Header_Len                               +
               Zone_State.Time_Count * Word_Bytes       +
               Zone_State.Time_Count                    +
               Zone_State.Type_Count * 6                +
               Zone_State.Char_Count                    +
               Zone_State.Leap_Count * (Word_Bytes + 4) +
               Std_Cnt                                  +
               GMT_Cnt
      then
         raise Unknown_Zone_Error;
      end if;

      -- Extract and convert the time data into the state info record's
      -- arrays.  Some consistency checks are done during the copy.  Start by
      -- setting the extraction cursor to point just past the header.
      Extract_Index := Header_Len;

      -- Get transition epochs
      for Time in 0 .. Zone_State.Time_Count - 1 loop
         Zone_State.Trans_Times (Time).Transition_At := Epoch_Value (Extract_Word);
      end loop;

      -- Get transition types, and verify that they're each within range.
      for Time in 0 .. Zone_State.Time_Count - 1 loop
         Zone_State.Trans_Times (Time).Transition_Type := Time_Type_Range (Extract_Byte);

         if Zone_State.Trans_Times (Time).Transition_Type >= Zone_State.Type_Count then
            raise Unknown_Zone_Error;
         end if;
      end loop;

      -- Get the part of the time-type info data that follows: GMT offset, DST
      -- flag, and abbrev index.
      for Ttype in 0 .. Zone_State.Type_Count - 1 loop
         Zone_State.Type_Infos (Ttype).GMT_Offset := Integer (Extract_Word);

         Tmp_Byte := Extract_Byte;

         if Tmp_Byte not in 0 .. 1 then
            raise Unknown_Zone_Error;
         end if;

         Zone_State.Type_Infos (Ttype).Is_DST := Tmp_Byte /= 0;
         Tmp_Byte := Extract_Byte;

         if Natural (Tmp_Byte) > Zone_State.Char_Count then
            raise Unknown_Zone_Error;
         end if;

         Zone_State.Type_Infos (Ttype).Abbrev_Index := Abbrev_Range (Tmp_Byte);
      end loop;

      -- Get zone name list, and ensure that there's at least one NUL at the end.
      for Char in 0 .. Zone_State.Char_Count - 1 loop
         Zone_State.Zone_Names (Char) := Character'Val (Natural (Extract_Byte) );
      end loop;

      Zone_State.Zone_Names (Zone_State.Char_Count) := Ada.Characters.Latin_1.NUL;

      -- Get leap-second info: transition time and correction amount.
      for Leap in 0 .. Zone_State.Leap_Count - 1 loop
         Zone_State.Leap_Secs (Leap).Transition_Time := Epoch_Value (Extract_Word);
         Zone_State.Leap_Secs (Leap).Correction      := Integer (Extract_Word);
      end loop;

      -- Fill in more of the time-type info data: standard vs. wall-clock time
      -- flags.
      for Ttype in 0 .. Zone_State.Type_Count - 1 loop
         if Std_Cnt = 0 then
            Zone_State.Type_Infos (Ttype).Is_Standard := False;
         else
            Tmp_Byte := Extract_Byte;

            if Tmp_Byte not in 0 .. 1 then
               raise Unknown_Zone_Error;
            end if;

            Zone_State.Type_Infos (Ttype).Is_Standard := Tmp_Byte /= 0;
         end if;
      end loop;

      -- Get the remaining time-type info data: GMT vs. local time flags.
      for Ttype in 0 .. Zone_State.Type_Count - 1 loop
         if GMT_Cnt = 0 then
            Zone_State.Type_Infos (Ttype).Is_GMT := False;
         else
            Tmp_Byte := Extract_Byte;

            if Tmp_Byte not in 0 .. 1 then
               raise Unknown_Zone_Error;
            end if;

            Zone_State.Type_Infos (Ttype).Is_GMT := Tmp_Byte /= 0;
         end if;
      end loop;

      -- Check the transition time ordering.  If they're not ascending, that
      -- should mean we're running on a system with signed epoch (time_t)
      -- values but using a data file with unsigned values (or vice versa).
      declare
         Time : Transition_Time_Info_Range := Zone_State.Trans_Times'First;
      begin
         Check_Transition_Time_Ordering : loop
            exit Check_Transition_Time_Ordering when Time >= Zone_State.Time_Count - 2;

            if Zone_State.Trans_Times (Time).Transition_At > Zone_State.Trans_Times (Time + 1).Transition_At then
               Zone_State.Time_Count := Time + 1;

               exit Check_Transition_Time_Ordering;
            end if;

            Time := Time + 1;
         end loop Check_Transition_Time_Ordering;
      end;

      -- Set the Go_Back and Go_Ahead flags to false, based on the fact that
      -- we are using only the 32-bit epoch values from the data files.
      Zone_State.Go_Back  := False;
      Zone_State.Go_Ahead := False;
   exception -- Load
      -- Can't swear that catching any and all constraint errors is the best
      -- idea, but it gives at least the same level of security/consistency as
      -- Olson's reference implementation, and actually a bit more.  If we pop
      -- any other kinds of exceptions, the code really needs to be
      -- re-examined closely to see what the heck is going on.
      when Constraint_Error =>
         raise Unknown_Zone_Error;
   end Load;

   procedure Names (Zone_Dir : in String := "") is
      Continue : Boolean := True;

      procedure Process_Zone_Names (Start : in String;  Prefix : in String) is

         function Useful_Name (Name : String) return Boolean is
         begin  -- Useful_Name
            if Name'Length > 0 and then Name (Name'First) = '.' then
               return False;
            end if;

            if Name'Length > 4 and then Name (Name'Last - 3 .. Name'Last) = ".tab" then
               return False;
            end if;

            return True;
         end Useful_Name;

         use Ada.Directories;

         Search : Search_Type;
         Find   : Directory_Entry_Type;

      begin  -- Process_Zone_Names
         Start_Search (Search, Start, "", (Directory | Ordinary_File => True, others => False) );

         All_Names : loop
            exit All_Names when not Continue or else not More_Entries (Search);

            Get_Next_Entry (Search, Find);

            declare
               Name : String := Simple_Name (Find);
            begin
               if Useful_Name (Name) then
                  if Kind (Find) = Directory then
                     if Prefix = "" then
                        Process_Zone_Names (Start => Full_Name (Find), Prefix => Name);
                     else
                        Process_Zone_Names (Start => Full_Name (Find), Prefix => Prefix & Dirsep & Name);
                     end if;
                  else
                     if Prefix = "" then
                        Process (Name => Name, Continue => Continue);
                     else
                        Process (Name => Prefix & Dirsep & Name, Continue => Continue);
                     end if;
                  end if;
               end if;
            end;
         end loop All_Names;
      end Process_Zone_Names;
   begin  -- Names
      -- Start with the given directory, if it's non-null.
      if Zone_Dir /= "" and then Ada.Directories.Exists (Zone_Dir) then
         Process_Zone_Names (Start => Zone_Dir, Prefix => "");

         return;
      end if;

      -- Given db directory not there, try the TZDIR environment variable.
      begin
         declare
            TZDIR : constant String := Ada.Environment_Variables.Value ("TZDIR");
         begin
            if TZDIR /= "" and then Ada.Directories.Exists (TZDIR) then
               Process_Zone_Names (Start => TZDIR, Prefix => "");

               return;
            end if;
         end;
      exception
         when others => -- Usually means the EV isn't set, but whatever the problem, it
            null;       -- sure isn't a usable location.  Move on to the next check.
      end;

      -- Database is not at TZDIR, try each directory in our built-in list.
      for Next in Zoneinfo_DB_Paths'Range loop
         declare
            --use Ada.Strings;

            Directory : constant String := Ada.Strings.Fixed.Trim (Zoneinfo_DB_Paths (Next), Side => Ada.Strings.Right);
         begin
            if Ada.Directories.Exists (Directory) then
               Process_Zone_Names (Start => Directory, Prefix => "");
               return;
            end if;
         end;
      end loop;
   end Names;
end Singo_RC.Zone.File;
