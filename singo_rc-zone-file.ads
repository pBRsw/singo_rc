------------------------------------------------------------------------------
-- Copyright (c) <2009>, <Singo Solution>, <AuroraUX Group>                 --
-- All rights reserved.                                                     --
--                                                                          --
-- Redistribution  and use  in source  and binary  forms,  with or without  --
-- modification,are permitted provided that the following conditions are met--
--                                                                          --
--     * Redistributions of source code must retain the above copyright     --
--       notice, this list of conditions and the following disclaimer.      --
--     * Redistributions in binary form must reproduce the above copyright  --
--       notice, this list of conditions and the following disclaimer in the--
--       documentation and/or other materials provided with the distribution--
--     * Neither the name of the <AuroraUX Group> nor the names of its      --
--       contributors may be used to endorse or promote products derived    --
--       from this software without specific prior written permission.      --
--                                                                          --
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS  --
-- IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,--
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR   --
-- PURPOSE ARE  DISCLAIMED.  IN NO  EVENT  SHALL THE  COPYRIGHT  HOLDER OR  --
-- CONTRIBUTORS  BE LIABLE FOR ANY DIRECT,  INDIRECT, INCIDENTAL, SPECIAL,  --
-- EXEMPLARY,  OR CONSEQUENTIAL  DAMAGES  (INCLUDING, BUT NOT LIMITED TO,   --
-- PROCUREMENT  OF SUBSTITUTE  GOODS OR  SERVICES;  LOSS OF USE,  DATA, OR  --
-- PROFITS; OR BUSINESS INTERRUPTION)  HOWEVER CAUSED AND ON ANY THEORY OF  --
-- LIABILITY,  WHETHER IN CONTRACT,  STRICT LIABILITY,  OR TORT (INCLUDING  --
-- NEGLIGENCE  OR  OTHERWISE) ARISING  IN ANY  WAY OUT  OF THE USE OF THIS  --
-- SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.             --
------------------------------------------------------------------------------

-- Low-level package for reading zoneinfo data files

package Singo_RC.Zone.File is
   Max_Times     : constant := 1200; -- Max number of transition times
   Max_Types     : constant :=  256; -- Max number of transition time types
   Max_Chars     : constant :=   50; -- Max number of abbreviation characters
   Max_Leaps     : constant :=   50; -- Max number of leap-second corrections
   Max_Zone_Name : constant :=  255; -- Max size of a zone name component
   -- These are defined in the latest Olson zoneinfo database code, as
   -- published at http://www.twinsun.com/tz/tz-link.htm.

   -- Note that zero-based arrays are widely used below, because many of the
   -- index values in the data assume it.

   subtype Abbrev_Range is Integer range 0 .. Max_Chars - 1;

   type Transition_Type_Info is record
      GMT_Offset   : Integer;      -- UTC offset in seconds
      Is_DST       : Boolean;      -- is daylight savings time
      Abbrev_Index : Abbrev_Range; -- abbreviation list index
      Is_Standard  : Boolean;      -- true if transition is std time
      Is_GMT       : Boolean;      -- true if transition is UTC
   end record;
   -- These values represent a single transition time's "type".

   subtype Time_Type_Range is Integer range 0 .. Max_Types - 1;

   type Time_Type_List is array (Time_Type_Range) of Transition_Type_Info;
   -- The list of all transition-time types in the file.

   type Transition_Time_Info is record
      Transition_At   : Epoch_Value;
      Transition_Type : Time_Type_Range;
   end record;

   subtype Transition_Time_Info_Range is Integer range 0 .. Max_Times - 1;

   type Transition_Time_List is array (Transition_Time_Info_Range) of Transition_Time_Info;
   -- The actual transition times recorded in a zoneinfo file; a combination
   -- of the Olson code's "ats" and "types".

   subtype Zone_Name_Range is Integer range 0 .. Max_Zone_Name;

   type Zone_Name_List is array (Zone_Name_Range) of Character;
   -- This array contains a list of NUL-terminated zone name abbreviations.
   -- Perhaps someday a more Ada-like presentation would be desirable.

   type Leap_Second_Adjustment is record
      Transition_Time : Epoch_Value; -- transition time
      Correction      : Integer;     -- correction to apply.
   end record;

   subtype Leap_Second_Adjustment_Range is Integer range 0 .. Max_Leaps - 1;

   type Leap_Second_Adjustment_List is array (Leap_Second_Adjustment_Range) of Leap_Second_Adjustment;
   -- The leap-second adjustments recorded in the zoneinfo file.

   type State_Info is record
      Leap_Count  : Natural;
      Time_Count  : Natural;
      Type_Count  : Natural;
      Char_Count  : Natural;
      Go_Back     : Boolean;
      Go_Ahead    : Boolean;
      Trans_Times : Transition_Time_List;
      Type_Infos  : Time_Type_List;
      Zone_Names  : Zone_Name_List;
      Leap_Secs   : Leap_Second_Adjustment_List;
   end record;
   -- This record is the return value for this package's lone public procedure.

   Unknown_Zone_Error : exception;
   -- We use the same exception name as the one used in the standard package
   -- Ada.Calendar.Time_Zones because (a) it describes the condition well, and
   -- (b) having a single exception parallels what the original Olson code
   -- does, summarily rejecting anything it doesn't like, causing the
   -- app-level code to fall back to GMT/UTC.  Having more detailed exceptions
   -- to describe various failure modes would not help that much, since
   -- there's little corrective action that can be taken.

   procedure Load (Zone_Name : in String; Zone_State : out State_Info; Zone_Dir : in String := "");
   -- Given a time zone's name, this procedure reads and returns the data
   -- found in that zone's zoneinfo data file.  Only returns the "long"
   -- (32-bit) time values; if the file has 64-bit values, they are ignored.
   --
   -- If the Zone_Dir parameter is non-null, it is used as a pathname for the
   -- zoneinfo database directory.  If the parameter is null, or the given
   -- zone file is not found there, the code next tries the TZDIR environment
   -- variable.  If that is null, or no file is found where it points, then we
   -- check a pre-set list of directories, including /usr/share/zoneinfo.
   --
   -- Raises Unknown_Zone_Error if the zone's data file cannot be found, or
   -- read, or is corrupted.

   generic -- Names
      with procedure Process (Name : in String; Continue : in out Boolean);
   procedure Names (Zone_Dir : in String := "");
   -- Calls Process with each available time zone name in turn, with Continue = True.  Returns
   -- immediately if Process sets Continue to False; any remaining names will not be returned.
   --
   -- If the Zone_Dir parameter is non-null, it is used as a pathname for the zoneinfo database
   -- directory.  If the parameter is null, or a directory is not found there, the code next tries
   -- the TZDIR environment variable.  If that is null, or no directory is found where it points,
   -- then we check a pre-set list of directories, including /usr/share/zoneinfo.
end Singo_RC.Zone.File;
